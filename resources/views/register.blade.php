<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 1 Berlatih HTML | Buat Akun Baru</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="POST">
        @csrf
        <p>
        <label>First Name:</label>
        <br><br>
        <input type="text" name="nama">
        </p>

        <p>
        <label>Last Name:</label>
        <br><br>
        <input type="text" name="namabelakang">
        </p>

        <p>
            <Label>Gender:</Label>
            <br>
            <p><input type="radio" name='jenis_kelamin' value="male">Man</p>
            <p><input type="radio" name='jenis_kelamin' value="female">Woman</p>
            <p><input type="radio" name='jenis_kelamin' value="other">Other</p>
        </p>

        <p>
            <label>Nationality:</label>
            <select name="nationality">
                <option value='indonesia'>Indonesia</option>
                <option value='malaysia'>Malaysia</option>
                <option value='australia'>Thailand</option>
            </select>
        </p>

        <p>
            <label>Language Spoken:</label>
            <p><input type="checkbox" name="b.indo" value="b.indo">Bahas Indonesia</p>
            <p><input type="checkbox" name="english" value="english">English</p>
            <p><input type="checkbox" name="other" value="other">Arabic</p>
            <p><input type="checkbox" name="other" value="other">Japanese</p>
        </p>

        <p>
            <label>Bio:</label>
            <br>
            <p> <textarea rows="10" cols="30" name="tentang"></textarea><br/>
                <input type="submit" name="submit" value="Sign Up"> </p>
        </p>
    </form>
</body>
</html>
