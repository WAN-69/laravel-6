@extends('adminlte.master')
@section('content')
<div class="row justify-content-center py-lg-5">
    <div class="card mb-3" style="max-width: 540px;">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img style="width: 100%; height:100%" src="{{ asset('img/Profile_avatar_placeholder_large.png') }}" alt="profile">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{ $cast->nama }}</h5>
                    <p class="card-text"><small class="text-muted">{{ $cast->umur }} Tahun</small></p>
                    <p class="card-text">{{$cast->bio}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection
