@extends('adminlte.master')
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Daftar Cast</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item active">
                            Cast
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3 class="card-title"></h3>
                <a href="/cast/create" class="btn btn-success"> <i class="fa fa-plus" aria-hidden="true"></i> Tambah</a>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip"
                        title="Remove">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-hover table-bordered">
                    <thead class="thead-light">
                        <tr class="text-center">
                            <th scope="col">No.Urut</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Umur</th>
                            <th scope="col">Bio</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($cast as $key=>$value)
                            <tr class="text-center">
                                <td>{{ $key + 1 }}</th>
                                <td>{{ $value->nama }}</td>
                                <td>{{ $value->umur }}</td>
                                <td class="text-break" style="width: 37rem;">{{ $value->bio }}</td>
                                <td>
                                    <form action="/cast/{{ $value->id }}" method="post">
                                        <a href="/cast/{{ $value->id }}" class="btn btn-xs btn-info my-1"><i
                                                class="fa fa-info-circle" aria-hidden="true"></i>Detail</a>
                                        <a href="/cast/{{ $value->id }}/edit" class="btn btn-xs btn-primary"> <i
                                                class="fas fa-edit    "></i> Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-xs btn-danger my-1"><i class="fas fa-trash"></i>Hapus</button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr colspan="3">
                                <td>Belum ada data</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div><!-- /.card -->
    </section>

@endsection

@push('scripts')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
        });
    </script>
@endpush
