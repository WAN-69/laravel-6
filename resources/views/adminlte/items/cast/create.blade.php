@extends('adminlte.master')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item">
                        <a href="#">Home</a>
                    </li>
                    <li class="breadcrumb-item active">
                        Tambah-Cast
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</section>
<section class="content">
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h1 class="card-title">Tambah Data Cast</h1>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Nama</label> <br>
                    @error('nama')
                    <div class="badge badge-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label> <br>
                    @error('umur')
                    <div class="badge badge-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan umur">
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label> <br>
                    @error('bio')
                    <div class="badge badge-danger">
                        {{ $message }}
                    </div>
                    @enderror
                    <textarea class="form-control" rows="10" cols="30" name="bio" id="bio" placeholder="Masukan Biodata"></textarea>
                </div>
                <div class="card-footer text-center"><button type="submit" class="btn btn-primary">Tambah</button></div>

            </form>
        </div>
    </div>
</section>
<!-- /.card -->
@endsection
