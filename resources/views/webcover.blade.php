<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Web Cover</title>
    <link href="{{ asset('adminlte/dist/css/adminlte.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body class="d-flex h-100 text-center text-white bg-dark" style="background-image: url('{{ asset('img/5570173.jpg') }}');  background-size: contain;">

<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
<header class="mb-auto">
    <div>
    <h3 class="float-md-start mb-0">Cover</h3>
    </div>
</header>

<main class="px-3">
    <h1>Selamat Datang di Web Cover saya !</h1>
    <p class="lead">Berikut saya serahkan tugas yang anda minta</p>
    <p class="lead">
    <a href="{{ url('home/') }}" class="btn btn-lg btn-light fw-bold border-black bg-black">Hari 12 – Membuat Web Statis dengan Laravel</a>
    <a href="{{ url('index/') }}" class="btn btn-lg btn-light fw-bold border-black bg-black">Hari 13 – Memasangkan Template dengan Laravel Blade</a>
    <a href="{{ url('cast/') }}" class="btn btn-lg btn-light fw-bold border-black bg-black mt-1">Hari 15 - Laravel CRUD dengan Query Builder</a>
    </p>
</main>

<footer class="mt-auto text-white-50">
</footer>
</div>
</body>
</html>
