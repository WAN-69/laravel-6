<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('webcover');
});

//Route Tugas Laravel Hari 12 – Membuat Web Statis dengan Laravel
Route::get(
    '/home',
    'HomeController@index'
);

Route::get(
    '/register',
    'AuthController@register'
);

Route::get(
    '/welcome',
    'AuthController@welcome'
);

Route::post(
    '/welcome',
    'AuthController@post_nama'
);

// Route Tugas Laravel Hari 13 – Memasangkan Template dengan Laravel Blade
Route::get('/index', function () {
    return view('adminlte.items.dashboard');
});

Route::get('/table', function () {
    return view('adminlte.items.table');
});

Route::get('/data-tables', function () {
    return view('adminlte.items.data-tables');
});

// Route Tugas Hari 15 - Laravel CRUD dengan Query Builder
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');
