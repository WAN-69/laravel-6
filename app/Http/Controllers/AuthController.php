<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome()
    {
        return view('wellcome');
    }

    public function post_nama(Request $request)
    {
        $nama = $request->nama;
        $namabelakang = $request->namabelakang;
        return view('wellcome', compact('nama', 'namabelakang'));
    }
}
